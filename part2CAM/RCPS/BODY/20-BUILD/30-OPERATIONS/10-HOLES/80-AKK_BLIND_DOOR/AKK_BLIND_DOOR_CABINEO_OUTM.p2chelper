(_FSET (_ 'halfOfThickness (/ __AD_PANELTHICK 2.0)))
(if (_EXISTPANEL AKK_BLIND_DOOR_CODE)
	(progn
		; IMPORTANT INFORMATION
		; 	To discriminate holes on each side of blind door shortkeys are used. Meaning of shortkeys, ds,DS -> Door side, bs,BS -> blind side
		; 	Door side assumed as front side and blind side assumed as back side. According to this assumption, rafix parameters are used
		
		; Blind door top-bottom loss parameters are taken via AKK_PARTS_LOSSES global variable which is set in AKK_MANUFACTURER.p2c
		(_FSET (_ 'topSideLoss (getnth 0 AKK_PARTS_LOSSES)))
		(_FSET (_ 'bottomSideLoss (getnth 1 AKK_PARTS_LOSSES)))
		; According to module direction, hole positions on X-axis are determined
		(cond
			((equal __MODULDIRECTION "L")
				(_FSET (_ 'bs_holePosOnAxisX (_= "AKK_BLIND_DOOR_WID - DISTANCE_TO_WALL - BLIND_DOOR_CONN_LEFT_OFFSET")))
				(_FSET (_ 'ds_holePosOnAxisX BLIND_DOOR_CONN_RIGHT_OFFSET))
			)
			((equal __MODULDIRECTION "R")
				(_FSET (_ 'bs_holePosOnAxisX (+ DISTANCE_TO_WALL BLIND_DOOR_CONN_LEFT_OFFSET)))
				(_FSET (_ 'ds_holePosOnAxisX (- AKK_BLIND_DOOR_WID BLIND_DOOR_CONN_RIGHT_OFFSET)))
			)
		)
		; Meaning of shortkeys, U -> Up, D -> Down
		(_FSET (_ 'holeCodeBody (_& (_ "BLIND_CORNER_BLIND_DOOR_RAFIX_HOLE_"))))
		(_CABINEOMAIN (_& (_ holeCodeBody "BSU")) AKK_BLIND_DOOR_CODE (_ (_ bs_holePosOnAxisX (_= "AKK_BLIND_DOOR_HEI + topSideLoss - halfOfThickness - CABINEO_HOR_DIAMETER") 0) CABINEO_PARAMETERS) nil)
		(_CABINEOMAIN (_& (_ holeCodeBody "BSD")) AKK_BLIND_DOOR_CODE (_ (_ bs_holePosOnAxisX (- (+ halfOfThickness CABINEO_HOR_DIAMETER) bottomSideLoss) 0) CABINEO_PARAMETERS) nil)
		(_CABINEOMAIN (_& (_ holeCodeBody "DSU")) AKK_BLIND_DOOR_CODE (_ (_ ds_holePosOnAxisX (_= "AKK_BLIND_DOOR_HEI + topSideLoss - halfOfThickness - CABINEO_HOR_DIAMETER") 0) CABINEO_PARAMETERS) nil)
		(_CABINEOMAIN (_& (_ holeCodeBody "DSD")) AKK_BLIND_DOOR_CODE (_ (_ ds_holePosOnAxisX (- (+ halfOfThickness CABINEO_HOR_DIAMETER) bottomSideLoss) 0) CABINEO_PARAMETERS) nil)
		(_ITEMMAIN CABINEO_CODE AKK_BLIND_DOOR_CODE (_ (* QUANTITY_OF_ITEM 4) CABINEO_UNIT))
	)
)
; IMPORTANT INFORMATION
; 	Priority of top panel is higher than front top strechers. If both of them exists by the way it is not logical, then operation performs on top panel
(cond
	((_EXISTPANEL TOP_PANEL_CODE)
		(cond
			((equal __MODULDIRECTION "L")
				(_FSET (_ 'bs_holePosOnAxisY (_= "TOP_PANEL_WID + __AD_PANELTHICK - BLIND_DOOR_CONN_LEFT_OFFSET")))
				(_FSET (_ 'ds_holePosOnAxisY (_= "TOP_PANEL_WID + __AD_PANELTHICK + DISTANCE_TO_WALL - AKK_BLIND_DOOR_WID + BLIND_DOOR_CONN_RIGHT_OFFSET")))
			)
			((equal __MODULDIRECTION "R")
				(_FSET (_ 'bs_holePosOnAxisY (- BLIND_DOOR_CONN_LEFT_OFFSET __AD_PANELTHICK)))
				(_FSET (_ 'ds_holePosOnAxisY (_= "AKK_BLIND_DOOR_WID - DISTANCE_TO_WALL - __AD_PANELTHICK - BLIND_DOOR_CONN_RIGHT_OFFSET")))
			)
		)
		(_FSET (_ 'holePosition_BS (_ (- TOP_PANEL_HEI 0) bs_holePosOnAxisY 0)))
		(_FSET (_ 'holePosition_DS (_ (- TOP_PANEL_HEI 0) ds_holePosOnAxisY 0)))
		
		(_CABINEOMAIN "BS" TOP_PANEL_OP_FACE (_ holePosition_BS CABINEO_PARAMETERS) "X+")
		(_CABINEOMAIN "DS" TOP_PANEL_OP_FACE (_ holePosition_DS CABINEO_PARAMETERS) "X+")
	)
	((_EXISTPANEL FRONT_TOP_STRECHER_CODE)
		(cond
			((equal __MODULDIRECTION "R")
				(_FSET (_ 'bs_holePosOnAxisX (- BLIND_DOOR_CONN_LEFT_OFFSET __AD_PANELTHICK)))
				(_FSET (_ 'ds_holePosOnAxisX (_= "AKK_BLIND_DOOR_WID - DISTANCE_TO_WALL - __AD_PANELTHICK - BLIND_DOOR_CONN_RIGHT_OFFSET")))
			)
			((equal __MODULDIRECTION "L")
				(_FSET (_ 'bs_holePosOnAxisX (_= "FRONT_TOP_STRECHER_WID + __AD_PANELTHICK - BLIND_DOOR_CONN_LEFT_OFFSET")))
				(_FSET (_ 'ds_holePosOnAxisX (_= "FRONT_TOP_STRECHER_WID + __AD_PANELTHICK + DISTANCE_TO_WALL - AKK_BLIND_DOOR_WID + BLIND_DOOR_CONN_RIGHT_OFFSET")))
			)
		)
		(_FSET (_ 'holePosition_BS (_ bs_holePosOnAxisX 0 0)))
		(_FSET (_ 'holePosition_DS (_ ds_holePosOnAxisX 0 0)))
		
		(_CABINEOMAIN "BS" FRONT_TOP_STRECHER_OP_FACE (_ holePosition_BS CABINEO_PARAMETERS) "Y+")
		(_CABINEOMAIN "DS" FRONT_TOP_STRECHER_OP_FACE (_ holePosition_DS CABINEO_PARAMETERS) "Y+")
	)
)
(if (_EXISTPANEL BOTTOM_PANEL_CODE)
	(progn
		(cond
			((equal __MODULDIRECTION "L")
				(_FSET (_ 'bs_holePosOnAxisY (_= "BOTTOM_PANEL_WID + bottomSideStyleV2 - BLIND_DOOR_CONN_LEFT_OFFSET")))
				(_FSET (_ 'ds_holePosOnAxisY (_= "BOTTOM_PANEL_WID + bottomSideStyleV2 + DISTANCE_TO_WALL - AKK_BLIND_DOOR_WID + BLIND_DOOR_CONN_RIGHT_OFFSET")))
			)
			((equal __MODULDIRECTION "R")
				(_FSET (_ 'bs_holePosOnAxisY (- BLIND_DOOR_CONN_LEFT_OFFSET bottomSideStyleV2)))
				(_FSET (_ 'ds_holePosOnAxisY (_= "AKK_BLIND_DOOR_WID - DISTANCE_TO_WALL - bottomSideStyleV2 - BLIND_DOOR_CONN_RIGHT_OFFSET")))
			)
		)
		(_FSET (_ 'holePosition_BS (_ 0 bs_holePosOnAxisY 0)))
		(_FSET (_ 'holePosition_DS (_ 0 ds_holePosOnAxisY 0)))
		
		(_CABINEOMAIN "BS" BOTTOM_PANEL_OP_FACE (_ holePosition_BS CABINEO_PARAMETERS) "X+")
		(_CABINEOMAIN "DS" BOTTOM_PANEL_OP_FACE (_ holePosition_DS CABINEO_PARAMETERS) "X+")
	)
)